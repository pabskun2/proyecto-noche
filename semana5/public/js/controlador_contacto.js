'use strict';
const input_nombre = document.querySelector('#txt_nombre');
const input_correo = document.querySelector('#txt_correo');
const fieldset_sexo = document.querySelector('#fieldset_sexo');
const text_comentario = document.querySelector('#txt_comentario');
const boton_enviar = document.querySelector('#btn_enviar');


let nombre = '';
let correo = '';
let sexo = '';
let comentario ='';


let validar = () =>{
    const boton_sexo = document.querySelector('input[name=rbt_sexo]:checked');
    let error = false; //False : no hay errores, true: hay errores

    if(input_nombre.value == ''){
        input_nombre.classList.add('error_input');
        error = true;
    }else{
        input_nombre.classList.remove('error_input');
    }

    if(input_correo.value == ''){
        input_correo.classList.add('error_input');
        error = true;
    }else{
        input_correo.classList.remove('error_input');
    }

    if(boton_sexo == null){
        fieldset_sexo.classList.add('error_input');
        error = true;
    }else{
        fieldset_sexo.classList.remove('error_input');
    }

    if(text_comentario.value == ''){
        text_comentario.classList.add('error_input');
        error = true;
    }else{
        text_comentario.classList.remove('error_input');
    }

    return error;
};

let mostrarDatos = () => {
    

    if( validar() == true ){//llamada a la función de validar
        Swal.fire({
            type: 'warning',
            title: 'Validación incorrecta',
            text: 'Por favor revise los campos resaltados en rojo' 
        })
    }else{
        nombre = input_nombre.value;
        correo = input_correo.value;
        sexo = document.querySelector('input[name=rbt_sexo]:checked').value;
        comentario = text_comentario.value;

        Swal.fire({
            type: 'success',
            title: 'Mensaje enviado con éxito',
            text: `Saludos ${nombre} usted es : ${sexo} y su comentario fue: "${comentario}" le estaremos respondiendo pronto al correo: ${correo}`
        })
        
    }
    

};


boton_enviar.addEventListener('click' , mostrarDatos);